## installer-config.txt
## Config for raspberrypi-ua-netinst image

## Package
preset=minimal
packages=nano,wget,unzip,systemd-sysv
release=stretch

## Device / peripheral 
spi_enable=1
i2c_enable=1

## SSH
username=clockpi
userpw=clockpi
userperms_admin=1
userperms_sound=1
usergpio=1
root_ssh_pwlogin=1
rootpw=raspbian

## Network
hostname=clockpi

## Localization
timezone=Europe/Berlin
keyboard_layout=de
system_default_locale=en_US.UTF-8

## Graphics / GPU
gpu_mem=16