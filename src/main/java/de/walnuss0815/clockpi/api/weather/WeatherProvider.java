package de.walnuss0815.clockpi.api.weather;

public abstract class WeatherProvider {
    public abstract WeatherResponse fetchWeather() throws Exception;
}
