package de.walnuss0815.clockpi.api.weather;

import com.google.gson.annotations.SerializedName;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;

public class OpenWeatherMapResponse {
    @SerializedName("coord")
    private final Coordinate coordinate;
    @SerializedName("weather")
    private final List<Weather> weathers;
    @SerializedName("base")
    private final String base;
    @SerializedName("main")
    private final Main main;
    @SerializedName("wind")
    private final Wind wind;
    @SerializedName("clouds")
    private final Clouds clouds;
    @SerializedName("rain")
    private final Rain rain;
    @SerializedName("snow")
    private final Snow snow;
    @SerializedName("dt")
    private final long time;
    @SerializedName("sys")
    private final Sys sys;
    @SerializedName("id")
    private final long cityId;
    @SerializedName("name")
    private final String cityName;
    @SerializedName("cod")
    private final int cod;

    public OpenWeatherMapResponse(Coordinate coordinate, List<Weather> weathers, String base, Main main, Wind wind, Clouds clouds, Rain rain, Snow snow, long time, Sys sys, long cityId, String cityName, int cod) {
        this.coordinate = coordinate;
        this.weathers = weathers;
        this.base = base;
        this.main = main;
        this.wind = wind;
        this.clouds = clouds;
        this.rain = rain;
        this.snow = snow;
        this.time = time;
        this.sys = sys;
        this.cityId = cityId;
        this.cityName = cityName;
        this.cod = cod;
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public List<Weather> getWeathers() {
        return weathers;
    }

    public String getBase() {
        return base;
    }

    public Main getMain() {
        return main;
    }

    public Wind getWind() {
        return wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public Rain getRain() {
        return rain;
    }

    public Snow getSnow() {
        return snow;
    }

    public long getTime() {
        return time;
    }

    public Sys getSys() {
        return sys;
    }

    public long getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public int getCod() {
        return cod;
    }

    public WeatherResponse toWeatherResponse() {
        LocalDateTime timestamp =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(this.getTime()),
                        TimeZone.getDefault().toZoneId());

        Float temperature = (this.getMain() != null) ? this.getMain().getTemperature() : null;
        Float pressure = (this.getMain() != null) ? this.getMain().getPressure() : null;
        Integer humidity = (this.getMain() != null) ? this.getMain().getHumidity() : null;
        Float windSpeed = (this.getWind() != null) ? this.getWind().getSpeed() : null;
        Float windDegree = (this.getWind() != null) ? this.getWind().getDegree() : null;
        Integer clouds = (this.getClouds() != null) ? this.getClouds().getAll() : null;
        Integer rain = (this.getRain() != null) ? this.getRain().getLastHour() : null;
        Integer snow = (this.getSnow() != null) ? this.getSnow().getLastHour() : null;

        return new WeatherResponse(timestamp, temperature, pressure, humidity, windSpeed, windDegree, clouds, rain, snow);
    }

    public class Coordinate {
        @SerializedName("lon")
        private final float longitude;
        @SerializedName("lat")
        private final float latitude;

        public Coordinate(float longitude, float latitude) {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public float getLongitude() {
            return longitude;
        }

        public float getLatitude() {
            return latitude;
        }
    }

    public class Weather {
        @SerializedName("id")
        private final int id;
        @SerializedName("main")
        private final String main;
        @SerializedName("description")
        private final String description;
        @SerializedName("icon")
        private final String icon;

        public Weather(int id, String main, String description, String icon) {
            this.id = id;
            this.main = main;
            this.description = description;
            this.icon = icon;
        }

        public int getId() {
            return id;
        }

        public String getMain() {
            return main;
        }

        public String getDescription() {
            return description;
        }

        public String getIcon() {
            return icon;
        }
    }

    public class Main {
        @SerializedName("temp")
        private final float temperature;
        @SerializedName("pressure")
        private final float pressure;
        @SerializedName("humidity")
        private final int humidity;
        @SerializedName("temp_min")
        private final float minTemp;
        @SerializedName("temp_max")
        private final float maxTemp;

        public Main(float temperature, float pressure, int humidity, float minTemp, float maxTemp) {
            this.temperature = temperature;
            this.pressure = pressure;
            this.humidity = humidity;
            this.minTemp = minTemp;
            this.maxTemp = maxTemp;
        }

        public float getTemperature() {
            return temperature;
        }

        public float getPressure() {
            return pressure;
        }

        public int getHumidity() {
            return humidity;
        }

        public float getMinTemp() {
            return minTemp;
        }

        public float getMaxTemp() {
            return maxTemp;
        }
    }

    public class Wind {
        @SerializedName("speed")
        private final float speed;
        @SerializedName("deg")
        private final float degree;

        public Wind(float speed, float degree) {
            this.speed = speed;
            this.degree = degree;
        }

        public float getSpeed() {
            return speed;
        }

        public float getDegree() {
            return degree;
        }
    }

    public class Clouds {
        @SerializedName("all")
        private final int all;

        public Clouds(int all) {
            this.all = all;
        }

        public int getAll() {
            return all;
        }
    }

    public class Rain {
        @SerializedName("1h")
        private final int lastHour;
        @SerializedName("3h")
        private final int last3Hours;

        public Rain(int lastHour, int last3Hours) {
            this.lastHour = lastHour;
            this.last3Hours = last3Hours;
        }

        public int getLastHour() {
            return lastHour;
        }

        public int getLast3Hours() {
            return last3Hours;
        }
    }

    public class Snow {
        @SerializedName("1h")
        private final int lastHour;
        @SerializedName("3h")
        private final int last3Hours;

        public Snow(int lastHour, int last3Hours) {
            this.lastHour = lastHour;
            this.last3Hours = last3Hours;
        }

        public int getLastHour() {
            return lastHour;
        }

        public int getLast3Hours() {
            return last3Hours;
        }
    }

    public class Sys {
        @SerializedName("type")
        private final int type;
        @SerializedName("id")
        private final long id;
        @SerializedName("message")
        private final float message;
        @SerializedName("country")
        private final String country;
        @SerializedName("sunrise")
        private final long sunrise;
        @SerializedName("sunset")
        private final long sunset;

        public Sys(int type, long id, float message, String country, long sunrise, long sunset) {
            this.type = type;
            this.id = id;
            this.message = message;
            this.country = country;
            this.sunrise = sunrise;
            this.sunset = sunset;
        }

        public int getType() {
            return type;
        }

        public long getId() {
            return id;
        }

        public float getMessage() {
            return message;
        }

        public String getCountry() {
            return country;
        }

        public long getSunrise() {
            return sunrise;
        }

        public long getSunset() {
            return sunset;
        }
    }
}
