package de.walnuss0815.clockpi.api.weather;

import com.google.gson.annotations.SerializedName;

import java.time.LocalDateTime;

public class WeatherResponse {
    @SerializedName("datetime")
    private final LocalDateTime timestamp;
    @SerializedName("temp")
    private final Float temperature;
    @SerializedName("pressure")
    private final Float pressure;
    @SerializedName("humidity")
    private final Integer humidity;
    @SerializedName("wind_speed")
    private final Float windSpeed;
    @SerializedName("wind_degree")
    private final Float windDegree;
    @SerializedName("clouds")
    private final Integer clouds;
    @SerializedName("rain")
    private final Integer rain;
    @SerializedName("snow")
    private final Integer snow;

    public WeatherResponse(LocalDateTime timestamp, Float temperature, Float pressure, Integer humidity, Float windSpeed, Float windDegree, Integer clouds, Integer rain, Integer snow) {
        this.timestamp = timestamp;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegree = windDegree;
        this.clouds = clouds;
        this.rain = rain;
        this.snow = snow;
    }

    public WeatherResponse(Float temperature, Float pressure, Integer humidity, Float windSpeed, Float windDegree, Integer clouds, Integer rain, Integer snow) {
        this.timestamp = LocalDateTime.now();
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDegree = windDegree;
        this.clouds = clouds;
        this.rain = rain;
        this.snow = snow;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public Float getTemperature() {
        return temperature;
    }

    public Float getPressure() {
        return pressure;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public Float getWindSpeed() {
        return windSpeed;
    }

    public Float getWindDegree() {
        return windDegree;
    }

    public Integer getClouds() {
        return clouds;
    }

    public Integer getRain() {
        return rain;
    }

    public Integer getSnow() {
        return snow;
    }
}
