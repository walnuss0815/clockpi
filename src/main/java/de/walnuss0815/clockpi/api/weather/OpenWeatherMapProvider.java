package de.walnuss0815.clockpi.api.weather;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class OpenWeatherMapProvider extends WeatherProvider {
    private final Coordinate coordinate;
    private final String token;
    private final UnitFormat unitFormat;

    public OpenWeatherMapProvider(double longitude, double latitude, String token, UnitFormat unitFormat) {
        this.token = token;
        this.unitFormat = unitFormat;
        coordinate = new Coordinate(longitude, latitude);
    }

    private static OpenWeatherMapResponse parseResponse(String json) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, OpenWeatherMapResponse.class);
    }

    public OpenWeatherMapResponse fetchOpenWeatherMapWeather() throws IOException {
        OkHttpClient client = new OkHttpClient();

        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.openweathermap.org/data/2.5/weather").newBuilder();
        urlBuilder.addQueryParameter("lat", Double.toString(this.coordinate.getLatitude()));
        urlBuilder.addQueryParameter("lon", Double.toString(this.coordinate.getLongitude()));
        urlBuilder.addQueryParameter("units", unitFormat.name().toLowerCase());
        urlBuilder.addQueryParameter("appid", this.token);
        String url = urlBuilder.build().toString();

        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();

        if ((response.body() == null)) {
            throw new IOException("Empty body");
        }

        return parseResponse(response.body().string());
    }

    public WeatherResponse fetchWeather() throws Exception {
        return this.fetchOpenWeatherMapWeather().toWeatherResponse();
    }

    public enum UnitFormat {
        METRIC,
        IMPERIAL
    }

    public class Coordinate {
        private final double longitude;
        private final double latitude;

        public Coordinate(double longitude, double latitude) {
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public double getLatitude() {
            return latitude;
        }
    }
}
