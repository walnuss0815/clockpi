package de.walnuss0815.clockpi;

import de.walnuss0815.clockpi.api.weather.OpenWeatherMapProvider;
import de.walnuss0815.clockpi.display.LedMatrixDisplay;
import de.walnuss0815.clockpi.font.PixelFont;
import de.walnuss0815.clockpi.util.AnalogDigitalConverter;
import de.walnuss0815.clockpi.util.BrightnessAdjuster;
import de.walnuss0815.clockpi.util.ViewManager;
import de.walnuss0815.clockpi.view.ClockView;
import de.walnuss0815.clockpi.view.WeatherView;

public class App {
    public App() {
        LedMatrixDisplay display;
        try {
            display = new LedMatrixDisplay(4, 90);
            display.turnOn();
            display.clear();

            AnalogDigitalConverter adc = new AnalogDigitalConverter();

            BrightnessAdjuster brightnessAdjuster = new BrightnessAdjuster(display, adc.getBrightnessSensor(), adc.getMaxValue());
            brightnessAdjuster.auto();

            ViewManager manager = new ViewManager(display);
            manager.addView(new ClockView(), 10000);
            manager.addView(new WeatherView(new OpenWeatherMapProvider(6.462710,
                    51.833780,
                    "dccd70fc726aa543c6a99bd2abfed8cc",
                    OpenWeatherMapProvider.UnitFormat.METRIC
            ), new PixelFont()), 10000);
            manager.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        App app = new App();
    }
}