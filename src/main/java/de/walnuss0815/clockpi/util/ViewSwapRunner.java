package de.walnuss0815.clockpi.util;

import de.walnuss0815.clockpi.display.LedMatrixDisplay;
import de.walnuss0815.clockpi.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ViewSwapRunner implements Runnable {
    private Logger LOG = LoggerFactory.getLogger(ViewSwapRunner.class);

    private ViewManager manager;
    private LedMatrixDisplay display;
    private boolean update;

    public ViewSwapRunner(ViewManager manager, LedMatrixDisplay display) {
        this.manager = manager;
        this.display = display;
        this.update = false;
    }

    @Override
    public synchronized void run() {
        View actual;
        View previous;
        long start;
        long duration;
        long timeout = 0;
        long beforeWait;
        boolean next;

        while (!Thread.interrupted()) {
            if (!this.update) {
                next = this.manager.next();
            } else {
                next = false;
            }

            try {
                start = System.currentTimeMillis();

                actual = (this.manager.getActual() != null) ? this.manager.getActual().getView() : null;
                previous = (this.manager.getPrevious() != null) ? this.manager.getPrevious().getView() : null;

                if (next || this.update) {
                    this.swap(1000, actual, previous, Transition.IMMEDIATELY);
                }

                duration = System.currentTimeMillis() - start;
                if (timeout == 0) {
                    timeout = (this.manager.getActual().getMills() > duration) ? (this.manager.getActual().getMills() - duration) : 1;
                }

                this.update = false;
                beforeWait = System.currentTimeMillis();
                this.wait(timeout);

                if (this.update) {
                    long waited = System.currentTimeMillis() - beforeWait;
                    timeout = (timeout > waited) ? timeout - waited : 1;
                } else {
                    timeout = 0;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void notifyUpdate() {
        this.update = true;
        this.notify();
    }

    private synchronized void swap(long duration, View newView, View oldView, Transition transition) throws IOException, InterruptedException {
        if (oldView == null) {
            if (newView != null) {
                this.display.setBuffer(newView.getData());
                this.display.flush();
                return;
            } else {
                return;
            }
        }

        LOG.debug("Swap from " + oldView.getClass().getName() + " to " + newView.getClass().getName() + "with transition " + transition.name());

        switch (transition) {
            case IMMEDIATELY:
                this.display.setBuffer(newView.getData());
                this.display.flush();
                break;
            case FADE:
                final int length = this.display.getBuffer().length;
                int a = 128;
                int b = 128;

                for (int j = 0; j < 8; j++) {
                    for (int i = 0; i < length; i++) {
                        if (Thread.interrupted()) {
                            this.display.setBuffer(newView.getData());
                            this.display.flush();
                        }

                        this.display.setByte(i + 1, (byte) (newView.getData()[i] & a));
                    }
                    this.display.flush();

                    b = b / 2;
                    a = a + b;

                    Thread.sleep(duration / 8);
                }
                break;
        }
    }

    public enum Transition {
        FADE,
        IMMEDIATELY
    }
}
