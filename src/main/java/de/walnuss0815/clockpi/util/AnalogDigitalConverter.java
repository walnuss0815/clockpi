package de.walnuss0815.clockpi.util;

import com.pi4j.gpio.extension.ads.ADS1115GpioProvider;
import com.pi4j.gpio.extension.ads.ADS1115Pin;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinAnalogInput;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CFactory;

import java.io.IOException;

public class AnalogDigitalConverter {
    private final GpioController gpio;
    private final ADS1115GpioProvider gpioProvider;
    private GpioPinAnalogInput brightnessSensor;

    public AnalogDigitalConverter() throws IOException, I2CFactory.UnsupportedBusNumberException {
        this.gpio = GpioFactory.getInstance();
        this.gpioProvider = new ADS1115GpioProvider(I2CBus.BUS_1, ADS1115GpioProvider.ADS1115_ADDRESS_0x48);
        this.init();
    }

    private void init() {
        this.brightnessSensor = gpio.provisionAnalogInputPin(gpioProvider, ADS1115Pin.INPUT_A0, "BrightnessSensor-A0");
    }

    public int getMaxValue() {
        return ADS1115GpioProvider.ADS1115_RANGE_MAX_VALUE;
    }

    public GpioPinAnalogInput getBrightnessSensor() {
        return brightnessSensor;
    }
}
