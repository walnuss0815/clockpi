package de.walnuss0815.clockpi.util;

import de.walnuss0815.clockpi.display.LedMatrixDisplay;
import de.walnuss0815.clockpi.view.View;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.NoSuchElementException;
import java.util.Observable;
import java.util.Observer;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ViewManager implements Observer {
    private Logger LOG = LoggerFactory.getLogger(ViewManager.class);

    private ViewSwapRunner runner;
    private Queue<ViewElement> queue;
    private ViewElement actual;
    private ViewElement previous;

    public ViewManager(LedMatrixDisplay display) {
        this.queue = new ConcurrentLinkedQueue<>();
        this.runner = new ViewSwapRunner(this, display);
    }

    /**
     * Run view manager
     */
    public void run() {
        if (this.queue.isEmpty()) {
            throw new NoSuchElementException("Queue is empty - Failed to run ViewSwapRunner");
        } else {
            Thread thread = new Thread(this.runner);
            thread.start();
        }
    }

    /**
     * Add view to queue
     *
     * @param view  View to add
     * @param mills Duration of the view
     * @param once  Show the view once
     */
    public void addView(View view, long mills, boolean once) {
        view.addObserver(this);
        queue.add(new ViewElement(view, mills, once));
    }

    /**
     * Add view to queue
     *
     * @param view  View to add
     * @param mills Duration of the view
     */
    public void addView(View view, long mills) {
        view.addObserver(this);
        queue.add(new ViewElement(view, mills));
    }

    /**
     * Show view now a single time
     *
     * @param view  View to be shown
     * @param mills Duration of the view
     */
    public synchronized void showViewNow(View view, long mills) {
        ViewElement element = new ViewElement(view, mills, true);
        this.previous = this.actual;
        this.actual = element;
        this.runner.notify();
        LOG.debug("Show view: " + view.getClass().getName() + " now");
    }

    public boolean next() {
        ViewElement preprevious = this.previous;
        this.previous = this.actual;

        try {
            this.actual = this.queue.remove();
        } catch (NoSuchElementException ex) {
            LOG.debug("Queue empty");
            this.previous = preprevious;
            return false;
        }

        if (!this.actual.isOnce()) {
            this.queue.add(this.actual);
        }

        return true;
    }

    public ViewElement getActual() {
        return actual;
    }

    public ViewElement getPrevious() {
        return previous;
    }

    @Override
    public void update(Observable o, Object arg) {
        try {
            View view = (View) o;
            LOG.debug("Update view: " + view.getClass().getName());

            if (this.actual == null) {
                return;
            }

            if (view == this.actual.getView()) {
                this.runner.notifyUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class ViewElement {
        private final View view;
        private final long mills;
        private final boolean once;

        public ViewElement(View view, long mills, boolean once) {
            this.view = view;
            this.mills = mills;
            this.once = once;
        }

        public ViewElement(View view, long mills) {
            this(view, mills, false);
        }

        public View getView() {
            return view;
        }

        public long getMills() {
            return mills;
        }

        public boolean isOnce() {
            return once;
        }
    }
}
