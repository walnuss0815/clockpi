package de.walnuss0815.clockpi.util;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinAnalogInput;
import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerAnalog;
import de.walnuss0815.clockpi.display.LedMatrixDisplay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BrightnessAdjuster implements GpioPinListenerAnalog {
    private Logger LOG = LoggerFactory.getLogger(BrightnessAdjuster.class);

    private LedMatrixDisplay display;
    private GpioPinAnalogInput sensor;
    private final int maxValue;

    public BrightnessAdjuster(LedMatrixDisplay display, GpioPinAnalogInput sensor, int maxValue) {
        this.display = display;
        GpioController gpio = GpioFactory.getInstance();
        this.sensor = sensor;
        this.maxValue = maxValue;
    }

    /**
     * Enables automatic brightness adjustment
     */
    public void auto() {
        if (!this.sensor.hasListener(this)) {
            this.sensor.addListener(this);
        }
    }

    /**
     * Enables manual brightness adjustment
     */
    public void manual() {
        if (this.sensor.hasListener(this)) {
            this.sensor.removeListener(this);
        }
    }

    @Override
    public void handleGpioPinAnalogValueChangeEvent(GpioPinAnalogValueChangeEvent event) {
        double value = event.getValue();
        double percent = 1.0 - (value / this.maxValue);

        // Correct percentage
        percent = (percent > 0.5) ? (percent - 0.5) * 2 : 0.0;

        int brightness = (percent > 0.5) ? (int) (15 * percent) : 0;

        LOG.debug("Percent: " + percent + "\nBrightness: " + brightness);

        try {
            this.display.setBrightness(brightness);
        } catch (Exception e) {
            LOG.error("Unable to set brightness", e);
        }
    }
}
