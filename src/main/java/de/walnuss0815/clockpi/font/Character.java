package de.walnuss0815.clockpi.font;

public class Character {
    private final int width;
    private final int[] data;

    public Character(int width, int[] data) {
        this.width = width;
        this.data = data;
    }

    public int getWidth() {
        return width;
    }

    public int[] getData() {
        return data;
    }
}
