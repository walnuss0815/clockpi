package de.walnuss0815.clockpi.font;

public abstract class Font {
    /*private static Character[] characters;

    public Character getCharacter(char ascii) {
        return characters[ascii];
    }*/

    public abstract Character getCharacter(char ascii);
}
