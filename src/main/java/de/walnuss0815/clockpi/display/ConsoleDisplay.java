package de.walnuss0815.clockpi.display;

public class ConsoleDisplay extends Display {

    public ConsoleDisplay(short cascaded, int rotation) {
        super(cascaded, rotation);
    }

    @Override
    public void sendCommand(byte command, byte data) {

    }

    @Override
    public void setByte(int position, byte value) {
        int offset = position - 1;
        this.buffer[offset] = value;
    }

    @Override
    protected void write(byte[] buffer) {

    }

    @Override
    public void clear() {

    }

    @Override
    public void flush() {

    }

    @Override
    public void setBrightness(int intensity) {

    }

    @Override
    public void shutdown() {

    }

    @Override
    public void turnOn() {

    }

    @Override
    protected byte[] values(int position, int[] buf) {
        return new byte[0];
    }
}
