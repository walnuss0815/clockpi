package de.walnuss0815.clockpi.display;

import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;

import java.io.IOException;

public class LedMatrixDisplay extends Display {
    private SpiDevice spi;

    /**
     * Construct display
     *
     * @param cascaded Count of cascaded displays
     * @param rotation Rotation of the display (must be 0, 90, 180 or 270)
     * @throws IOException
     */
    public LedMatrixDisplay(int cascaded, int rotation) throws Exception {
        super(cascaded, rotation);
        this.spi = SpiFactory.getInstance(SpiChannel.CS0, SpiDevice.DEFAULT_SPI_SPEED, SpiDevice.DEFAULT_SPI_MODE);
        sendCommand(Command.MAX7219_REG_SCANLIMIT, (byte) 0x7);
        sendCommand(Command.MAX7219_REG_DECODEMODE, (byte) 0x0);
        sendCommand(Command.MAX7219_REG_DISPLAYTEST, (byte) 0x0);
        this.setBrightness((byte) 3);

        LOG.info("Matrix display initialized - Cascaded: " + this.cascaded);
    }

    /**
     * Construct single 8x8 matrix display with rotation 0
     *
     * @throws IOException
     */
    public LedMatrixDisplay() throws Exception {
        this((short) 1, 0);
    }

    /**
     * Send command to display
     *
     * @param command Command to send
     * @param data
     * @throws IOException
     */
    public void sendCommand(byte command, byte data) throws IOException {

        int len = 2 * this.cascaded;
        byte[] buffer = new byte[len];

        for (int i = 0; i < len; i += 2) {
            buffer[i] = command;
            buffer[i + 1] = data;
        }

        this.write(buffer);
    }

    /**
     * Set byte of buffer
     *
     * @param position Position in buffer array
     * @param value    Value which will be set
     */
    public void setByte(int position, byte value) {
        int offset = position - LedMatrixDisplay.Command.MAX7219_REG_DIGIT0;
        this.buffer[offset] = value;
    }

    /**
     * Get byte of buffer
     *
     * @param position Position in buffer array
     */
    public int getByte(int position) {
        int offset = position - LedMatrixDisplay.Command.MAX7219_REG_DIGIT0;
        return this.buffer[offset];
    }

    /**
     * Write buffer to display
     *
     * @param buffer Buffer which will be written on the display
     * @throws IOException
     */
    protected void write(byte[] buffer) throws IOException {
        this.spi.write(buffer);
    }

    /**
     * Clear display
     */
    public void clear() throws Exception {
        for (int i = 0; i < this.cascaded; i++) {
            for (short j = 0; j < NUM_DIGITS; j++) {
                this.setByte((short) (i * NUM_DIGITS + j + Command.MAX7219_REG_DIGIT0), (byte) 0x00);
            }
        }

        this.flush();
    }

    /**
     * Flush buffer to display
     *
     * @throws IOException
     */
    public void flush() throws IOException {
        int[] buf = this.buffer;

        if (this.rotation > 0) {
            buf = this.rotate(buf);
        }

        for (short pos = 0; pos < NUM_DIGITS; pos++) {
            this.write(this.values(pos, buf));
        }
    }

    /**
     * Set brightness
     *
     * @param brightness Display intensity (must be greater than 0 and less than 15)
     */
    public void setBrightness(int brightness) throws Exception {
        if (brightness < 0 || brightness > 15) {
            throw new IllegalArgumentException("Intensity must be greater than 0 and less than 15");
        }

        this.sendCommand(Command.MAX7219_REG_INTENSITY, (byte) brightness);
    }

    /**
     * Shutdown display
     *
     * @throws Exception
     */
    public void shutdown() throws Exception {
        this.sendCommand(Command.MAX7219_REG_SHUTDOWN, (byte) 0x0);
    }

    /**
     * Turn on display
     *
     * @throws Exception
     */
    public void turnOn() throws Exception {
        this.sendCommand(Command.MAX7219_REG_SHUTDOWN, (byte) 0x1);
    }

    protected byte[] values(int position, int[] buf) {
        int len = 2 * this.cascaded;
        byte[] ret = new byte[len];

        for (int i = 0; i < this.cascaded; i++) {
            ret[2 * i] = (byte) ((position + Command.MAX7219_REG_DIGIT0) & 0xff);
            ret[2 * i + 1] = (byte) buf[(i * NUM_DIGITS) + position];

        }
        return ret;
    }

    private static class Command {
        public static byte MAX7219_REG_NOOP = 0x0;
        public static byte MAX7219_REG_DIGIT0 = 0x1;
        public static byte MAX7219_REG_DIGIT1 = 0x2;
        public static byte MAX7219_REG_DIGIT2 = 0x3;
        public static byte MAX7219_REG_DIGIT3 = 0x4;
        public static byte MAX7219_REG_DIGIT4 = 0x5;
        public static byte MAX7219_REG_DIGIT5 = 0x6;
        public static byte MAX7219_REG_DIGIT6 = 0x7;
        public static byte MAX7219_REG_DIGIT7 = 0x8;
        public static byte MAX7219_REG_DECODEMODE = 0x9;
        public static byte MAX7219_REG_INTENSITY = 0xA;
        public static byte MAX7219_REG_SCANLIMIT = 0xB;
        public static byte MAX7219_REG_SHUTDOWN = 0xC;
        public static byte MAX7219_REG_DISPLAYTEST = 0xF;
    }
}
