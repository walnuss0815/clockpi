package de.walnuss0815.clockpi.display;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Display {
    protected static final int NUM_DIGITS = 8;
    protected Logger LOG = LoggerFactory.getLogger(Display.class);
    protected int cascaded;
    protected int rotation;
    protected int[] buffer;

    /**
     * Construct display
     *
     * @param cascaded Count of cascaded displays
     * @param rotation Rotation of the display (must be 0, 90, 180 or 270)
     */
    public Display(int cascaded, int rotation) {
        this.cascaded = cascaded;
        this.setRotation(rotation);
        this.buffer = new int[NUM_DIGITS * this.cascaded];
    }

    /**
     * Rotation of the display (must be 0, 90, 180 or 270)
     *
     * @param rotation Rotation of the display
     */
    public void setRotation(int rotation) {
        if (rotation != 0 && rotation != 90 && rotation != 180 && rotation != 270) {
            throw new IllegalArgumentException("Degree must be 0, 90, 180 or 270");
        }

        this.rotation = rotation;
    }

    /**
     * Send command to display
     *
     * @param command
     * @param data
     * @throws Exception
     */
    public abstract void sendCommand(byte command, byte data) throws Exception;

    /**
     * @param position
     * @param value
     */
    public abstract void setByte(int position, byte value);

    /**
     * Write buffer to display
     *
     * @param buffer
     * @throws Exception
     */
    protected abstract void write(byte[] buffer) throws Exception;

    /**
     * Clear display
     */
    public abstract void clear() throws Exception;

    /**
     * Flush buffer to display
     */
    public abstract void flush() throws Exception;

    /**
     * Set brightness
     *
     * @param intensity Display intensity (must be greater than 0 and less than 15)
     */
    public abstract void setBrightness(int intensity) throws Exception;

    /**
     * Shutdown display
     *
     * @throws Exception
     */
    public abstract void shutdown() throws Exception;

    /**
     * Turn on display
     *
     * @throws Exception
     */
    public abstract void turnOn() throws Exception;

    protected int[] rotate8x8(int[] buf) {
        int[] result = new int[8];

        for (int i = 0; i < 8; i++) {
            short b = 0;
            short t = (short) ((0x01 << i) & 0xff);
            for (int j = 0; j < 8; j++) {
                int d = 7 - i - j;
                if (d > 0)
                    b += (short) ((buf[j] & t) << d);
                else
                    b += (short) ((buf[j] & t) >> (-1 * d));
            }

            result[i] = b;
        }

        return result;
    }

    protected int[] rotate(int[] buf) {
        int[] result = new int[this.buffer.length];

        for (int i = 0; i < this.cascaded * NUM_DIGITS; i += NUM_DIGITS) {
            int[] tile = new int[NUM_DIGITS];
            for (int j = 0; j < NUM_DIGITS; j++) {
                tile[j] = buf[i + j];
            }

            int k = this.rotation / 90;
            for (int j = 0; j < k; j++) {
                tile = this.rotate8x8(tile);
            }

            for (int j = 0; j < NUM_DIGITS; j++) {
                result[i + j] = tile[j];
            }
        }

        return result;
    }

    public int[] getBuffer() {
        return this.buffer;
    }

    public void setBuffer(int[] buffer) {
        this.buffer = buffer;
    }

    protected abstract byte[] values(int position, int[] buf);
}
