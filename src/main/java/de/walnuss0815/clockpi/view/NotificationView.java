package de.walnuss0815.clockpi.view;

import de.walnuss0815.clockpi.font.Character;
import de.walnuss0815.clockpi.font.Font;

public class NotificationView extends TextView {
    private Character icon;

    public NotificationView(String text, Font font, Character icon) {
        super(text, font);
        this.icon = icon;
        this.updateBuffer(icon.getWidth(), DISPLAY_WIDTH);
        this.update();
    }

    @Override
    protected void init() {
        this.updateTextBuffer();
    }

    @Override
    protected void updateBuffer(int start, int end) {
        System.arraycopy(this.icon.getData(), 0, this.data, 0, this.icon.getWidth());
        super.updateBuffer(this.icon.getWidth() + 1, end);
    }

    private void updateIconBuffer() {
        System.arraycopy(this.icon.getData(), 0, this.data, 0, this.icon.getWidth());
        //for (int i = 0; i < icon.getWidth(); i++) {
        //    this.data[i] = this.icon.getData()[i];
        //}
    }
}
