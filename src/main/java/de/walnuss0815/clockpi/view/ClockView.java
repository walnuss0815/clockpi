package de.walnuss0815.clockpi.view;

import de.walnuss0815.clockpi.font.Character;
import de.walnuss0815.clockpi.font.ClockFont;
import de.walnuss0815.clockpi.font.Font;
import de.walnuss0815.clockpi.view.runner.ClockViewRunner;

import java.time.LocalDateTime;

public class ClockView extends View {
    private static final int SEPARATOR_POSITION = 16;
    private static final int DIGIT_WIDTH = 4;

    private final Font font;

    public ClockView() {
        font = new ClockFont();

        this.setTime(LocalDateTime.now());

        ClockViewRunner runner = new ClockViewRunner(this);
        Thread thread = new Thread(runner);
        thread.start();
    }

    public void setTime(LocalDateTime time) {
        int h1 = time.getHour() % 10;
        int h10 = time.getHour() / 10;

        int m1 = time.getMinute() % 10;
        int m10 = time.getMinute() / 10;

        insertDigit(SEPARATOR_POSITION - DIGIT_WIDTH - DIGIT_WIDTH - 2, (char) (h10 + (int) '0'));
        insertDigit(SEPARATOR_POSITION - DIGIT_WIDTH - 1, (char) (h1 + (int) '0'));
        insertDigit(SEPARATOR_POSITION + 2, (char) (m10 + (int) '0'));
        insertDigit(SEPARATOR_POSITION + DIGIT_WIDTH + 3, (char) (m1 + (int) '0'));
        insertSeparator();
    }

    public void update() {
        setChanged();
        notifyObservers();
    }

    private void insertDigit(int startIndex, char digit) {
        Character character = this.font.getCharacter(digit);

        for (int i = 0; i < DIGIT_WIDTH; i++) {
            if (i < character.getData().length) {
                this.data[i + startIndex] = character.getData()[i];
            } else {
                this.data[i + startIndex] = 0x00;
            }
        }
    }

    private void insertSeparator() {
        this.data[SEPARATOR_POSITION] = this.font.getCharacter(':').getData()[0];
    }
}
