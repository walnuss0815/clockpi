package de.walnuss0815.clockpi.view;

import java.util.Observable;

public abstract class View extends Observable {
    protected int[] data = new int[32];

    public int[] getData() {
        return data;
    }
}
