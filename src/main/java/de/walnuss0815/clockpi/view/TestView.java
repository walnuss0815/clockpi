package de.walnuss0815.clockpi.view;

/**
 * A view with all LEDs switched on
 */
public class TestView extends View {
    public TestView() {
        for (int i = 0; i < this.data.length; i++) {
            this.data[i] = 0xFF;
        }
    }
}
