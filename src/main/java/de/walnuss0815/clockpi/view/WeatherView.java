package de.walnuss0815.clockpi.view;

import de.walnuss0815.clockpi.api.weather.WeatherProvider;
import de.walnuss0815.clockpi.font.Font;
import de.walnuss0815.clockpi.view.runner.WeatherRunner;

public class WeatherView extends TextView {
    private Thread thread;

    public WeatherView(WeatherProvider provider, Font font) {
        super("--°C", font);

        WeatherRunner runner = new WeatherRunner(provider, this);
        this.thread = new Thread(runner);
        this.thread.setName("WeatherRunner");
        this.thread.start();
    }

    public void setTemperature(Float temperature) {
        this.setText(String.format("%,.2f", temperature) + "°C");
    }

    @Override
    protected synchronized void finalize() throws Throwable {
        this.thread.interrupt();
        super.finalize();
    }
}