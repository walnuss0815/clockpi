package de.walnuss0815.clockpi.view;

public class TestMatrixView extends View {
    public TestMatrixView() {
        for (int i = 0; i < this.data.length; i++) {
            if (i % 2 == 0) {
                this.data[i] = 0xAA;
            } else {
                this.data[i] = 0x55;
            }
        }
    }
}