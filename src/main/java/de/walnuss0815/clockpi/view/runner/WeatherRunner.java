package de.walnuss0815.clockpi.view.runner;

import de.walnuss0815.clockpi.api.weather.WeatherProvider;
import de.walnuss0815.clockpi.api.weather.WeatherResponse;
import de.walnuss0815.clockpi.view.WeatherView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class WeatherRunner implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(WeatherRunner.class);

    private WeatherProvider provider;
    private WeatherView view;
    private long timeout;

    public WeatherRunner(WeatherProvider provider, WeatherView view, long timeout) {
        this.provider = provider;
        this.view = view;
        this.timeout = timeout;
    }

    public WeatherRunner(WeatherProvider provider, WeatherView view) {
        this.provider = provider;
        this.view = view;
        this.timeout = 120_000; // 2 minutes
    }

    @Override
    public synchronized void run() {
        while (!Thread.interrupted()) {
            try {
                try {
                    WeatherResponse response = this.provider.fetchWeather();
                    this.view.setTemperature(response.getTemperature());
                    wait(this.timeout);
                } catch (IOException e) {
                    LOG.info("Error - Trying again in 60 seconds");
                    LOG.error(e.getMessage(), e);
                    wait(60_000);
                } catch (Exception e) {
                    this.view.setText("Err°C");
                    LOG.info("Error - Trying again in 60 seconds");
                    LOG.error(e.getMessage(), e);
                    wait(60_000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
