package de.walnuss0815.clockpi.view.runner;

import de.walnuss0815.clockpi.view.ClockView;

import java.time.LocalDateTime;

public class ClockViewRunner implements Runnable {
    private ClockView view;

    public ClockViewRunner(ClockView view) {
        this.view = view;
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            LocalDateTime time = LocalDateTime.now();
            this.view.setTime(time);
            this.view.update();

            time = LocalDateTime.now();
            try {
                Thread.sleep(mills(time));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private long mills(LocalDateTime time) {
        return (60 - time.getSecond()) * 1000;
    }
}
