package de.walnuss0815.clockpi.view;

import de.walnuss0815.clockpi.font.Character;
import de.walnuss0815.clockpi.font.Font;

public class TextView extends View {
    protected final static int DISPLAY_WIDTH = 32;

    protected String text;
    protected int[] textBuffer;
    protected Font font;

    public TextView(String text, Font font) {
        this.text = text;
        this.font = font;
        this.init();
    }

    protected void init() {
        this.updateTextBuffer();
        this.updateBuffer(0, DISPLAY_WIDTH);
        this.update();
    }

    protected void updateTextBuffer() {
        final int length = this.calculateLength(this.text);
        this.textBuffer = new int[length];

        int index = 0;
        for (char c : this.text.toCharArray()) {
            Character character = this.font.getCharacter(c);

            for (int i = 0; i < character.getWidth(); i++) {
                this.textBuffer[i + index] = character.getData()[i];
            }
            index += (character.getWidth() + 1);
        }
    }

    public void update() {
        setChanged();
        notifyObservers();
    }

    protected int calculateLength(String text) {
        int length = 0;
        for (char c : text.toCharArray()) {
            length += (this.font.getCharacter(c).getWidth() + 1);
        }

        return length;
    }

    /**
     * Update buffer
     *
     * @param start Start index of text buffer
     * @param end   End index of text buffer
     */
    protected void updateBuffer(int start, int end) {
        for (int i = start; i < end; i++) {
            if (i < this.textBuffer.length + start) {
                this.data[i] = this.textBuffer[i - start];
            } else {
                this.data[i] = 0x00;
            }
        }
    }

    public void setFont(Font font) {
        this.font = font;
        this.updateTextBuffer();
        this.updateBuffer(0, DISPLAY_WIDTH);
        this.update();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        this.updateTextBuffer();
        this.updateBuffer(0, DISPLAY_WIDTH);
        this.update();
    }
}
