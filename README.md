# ClockPi

![clock](doc/img/clock.jpg)

## Requirements
* Raspberry Pi
* Matrix LED Display (MAX7219)
* Analog digital converter (ADS1115)


## Wireing
### Display
#### Raspberry Pi Model B (Revision 2.0)
| Board Pin | Name             | RPi Pin | RPi Function      |
| --------- | ---------------- | ------- | ----------------- |
| 1         | +5V Power (VCC)  | 2       | 5V0               |
| 2         | Ground (GND)     | 6       | GND               |
| 3         | Data In (DIN)    | 19      | GPIO 10 (MOSI)    |
| 4         | Chip Select (CS) | 24      | GPIO 8 (SPI CE0)  |
| 5         | Clock (CLK)      | 23      | GPIO 11 (SPI CLK) |

#### Raspberry Pi Model B+
| Board Pin | Name             | RPi Pin | RPi Function      |
| --------- | ---------------- | ------- | ----------------- |
| 1         | +5V Power (VCC)  | 2 or 4  | 5V0               |
| 2         | Ground (GND)     | 6       | GND               |
| 3         | Data In (DIN)    | 19      | GPIO 12 (MOSI)    |
| 4         | Chip Select (CS) | 24      | GPIO 10 (SPI CE0)  |
| 5         | Clock (CLK)      | 23      | GPIO 14 (SPI CLK) |

### ADC
#### Raspberry Pi Model B (Revision 2.0)
| Board Pin | Name             | RPi Pin | RPi Function      |
| --------- | ---------------- | ------- | ----------------- |
| 1         | VCC              | 17      | 3V3               |
| 2         | GND              | 20      | GND               |
| 3         | SCL              | 5       | SCL I2C           |
| 4         | SDA              | 3       | SDA I2C           |
| 5         | ADDR             | 9       | GND               |
| 6         | ALRT             | -       | -                 |